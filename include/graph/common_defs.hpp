#ifndef GRAPH_COMMON_DEFS_HPP_198949CD2E0347328A53BBC804EFECFC
#define GRAPH_COMMON_DEFS_HPP_198949CD2E0347328A53BBC804EFECFC

#include <cstddef>
#include "NamedType/named_type.hpp"

namespace graph {
    inline namespace enums {
        // Enum to differentiate several types (handles, ids...) belonging to either nodes or edges
        enum GraphMemberKind : unsigned char { e_Node = 0, e_Edge = 1 };

        enum EdgeDirection : unsigned char { e_Incoming = 0, e_Outgoing = 1 };
    }  // namespace enums

    // Some technical types that are not to be seen outside
    namespace detail {
        template<GraphMemberKind>
        struct id_tag;
    }  // namespace detail

    template<GraphMemberKind kind>
    using id_t = fluent::NamedType<std::size_t, detail::id_tag<kind>, fluent::Arithmetic>;

    using node_id_t = id_t<e_Node>;
    using edge_id_t = id_t<e_Edge>;

    // Own namespace so they can be used without using namespace graph;
    inline namespace literals {
        constexpr auto operator"" _node(unsigned long long id) -> node_id_t {
            return node_id_t{id};
        }

        constexpr auto operator"" _edge(unsigned long long id) -> edge_id_t {
            return edge_id_t{id};
        }
    }  // namespace literals
}  // namespace graph

#endif  // GRAPH_COMMON_DEFS_HPP_198949CD2E0347328A53BBC804EFECFC
