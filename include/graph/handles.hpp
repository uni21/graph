#ifndef GRAPH_HANDLES_HPP_FEFD201E6B28479BBFC808E27B549FA1
#define GRAPH_HANDLES_HPP_FEFD201E6B28479BBFC808E27B549FA1

#include "detail/base_handle.hpp"
#include "detail/node_handle.hpp"
#include "detail/edge_handle.hpp"
#include "detail/helpers.hpp"

namespace graph {
    template<GraphMemberKind kind, class Graph>
    using wrapped_t = detail::enum_switch_t<kind, detail::NodeWrapped<Graph>, detail::EdgeWrapped<Graph>>;

    template<GraphMemberKind kind, class Graph>
    using handle_t = detail::BaseHandle<wrapped_t<kind, Graph>>;

    template<class Graph>
    using NodeHandle = handle_t<e_Node, Graph>;

    template<class Graph>
    using EdgeHandle = handle_t<e_Edge, Graph>;
}  // namespace graph

#endif  // GRAPH_HANDLES_HPP_FEFD201E6B28479BBFC808E27B549FA1
