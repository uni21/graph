#ifndef GRAPH_EDGE_HPP_1EE572AE0EDB4C0BA8BA30DC38022860
#define GRAPH_EDGE_HPP_1EE572AE0EDB4C0BA8BA30DC38022860

#include "common_defs.hpp"
#include "detail/helpers.hpp"
#include "detail/graph_members.hpp"
namespace graph {
    template<class Label>
    struct Edge : private detail::empty_base<Label> {
        using label_t = Label;
        using id_t = edge_id_t;

        template<class... Args>
        Edge(node_id_t from, node_id_t to, Args &&...args) :
                base(std::forward<Args>(args)...), m_from(from), m_to(to) {}

        template<class TempLabel = label_t, class = std::enable_if_t<!std::is_same_v<TempLabel, void>>>
        auto label() const -> TempLabel {
            return base::data;
        }

        template<class TempLabel = label_t, class = std::enable_if_t<!std::is_same_v<TempLabel, void>>>
        auto label() -> TempLabel & {
            return base::data;
        }

        template<class Func, class Res = std::invoke_result_t<Func, node_id_t>>
        auto apply_to_nodes(Func &&f) -> std::conditional_t<std::is_same_v<Res, void>, void, std::pair<Res, Res>> {
            if constexpr (std::is_same_v<Res, void>) {
                f(m_from);
                f(m_to);
            } else {
                return {f(m_from), f(m_to)};
            }
        }

        node_id_t m_from;
        node_id_t m_to;

      private:
        using base = detail::empty_base<Label>;
    };

    template<>
    struct detail::GraphMember<e_Edge> {
        template<class Label>
        using member_t = Edge<Label>;
    };
}  // namespace graph
#endif  // GRAPH_EDGE_HPP_1EE572AE0EDB4C0BA8BA30DC38022860
