#ifndef GRAPH_STORE_HPP_126BFEEDCE0A4E519A14F7E1132F5D53
#define GRAPH_STORE_HPP_126BFEEDCE0A4E519A14F7E1132F5D53

#include "common_defs.hpp"
#include <type_traits>
#include <vector>

namespace graph {
    template<class Stored, class Graph, GraphMemberKind kind>
    struct Store {
        using stored_t = Stored;
        using graph_t = Graph;
        static constexpr GraphMemberKind member_kind = kind;
        using index_t = graph::id_t<member_kind>;

        Store(const Graph &g, const stored_t &default_value) :
                m_stored(g.template num_members<kind>().get(), default_value) {}

        template<class TempStored = Stored, class = std::enable_if_t<std::is_default_constructible_v<TempStored>>>
        explicit Store(const Graph &g) : Store(g, TempStored{}) {}

        auto operator[](index_t index) -> stored_t & {
            return m_stored[index.get()];
        }

        auto operator[](index_t index) const -> const stored_t & {
            return m_stored[index.get()];
        }

        auto operator[](handle_t<member_kind, const graph_t> handle) -> stored_t & {
            return (*this)[handle.get_id()];
        }

        auto operator[](handle_t<member_kind, const graph_t> handle) const -> const stored_t & {
            return (*this)[handle.get_id()];
        }

        auto size() const {
            return m_stored.size();
        }

        auto begin() {
            return std::begin(m_stored);
        }

        auto begin() const {
            return std::begin(m_stored);
        }

        auto end() {
            return std::end(m_stored);
        }

        auto end() const {
            return std::end(m_stored);
        }

      private:
        std::vector<Stored> m_stored;
    };

    template<class Stored, class Graph>
    using node_store_t = Store<Stored, Graph, GraphMemberKind::e_Node>;
    template<class Stored, class Graph>
    using edge_store_t = Store<Stored, Graph, GraphMemberKind::e_Edge>;
}  // namespace graph

#endif  // GRAPH_STORE_HPP_126BFEEDCE0A4E519A14F7E1132F5D53
