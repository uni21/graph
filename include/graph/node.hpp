#ifndef GRAPH_NODE_HPP_D9A11A6CC27A45B69F037C6FD6FC1183
#define GRAPH_NODE_HPP_D9A11A6CC27A45B69F037C6FD6FC1183

#include "common_defs.hpp"
#include "detail/helpers.hpp"
#include "detail/graph_members.hpp"
#include "nanorange.hpp"
#include <algorithm>
#include <cassert>
#include <functional>
#include <type_traits>
#include <vector>

namespace graph {
    template<class Label>
    struct Node : detail::empty_base<Label> {
        using label_t = Label;
        using id_t = node_id_t;

        template<class... Args, class = std::enable_if_t<!std::is_invocable_v<Node(const Node &), Args...>>>
        explicit Node(Args &&...args) : base(std::forward<Args>(args)...) {}

        template<class TempLabel = label_t, class = std::enable_if_t<!std::is_same_v<TempLabel, void>>>
        auto label() const -> const TempLabel& {
            return base::data;
        }

        template<class TempLabel = label_t, class = std::enable_if_t<!std::is_same_v<TempLabel, void>>>
        auto label() -> TempLabel & {
            return base::data;
        }

        auto incoming_edges() -> nano::ref_view<std::vector<edge_id_t>> {
            return nano::ref_view{m_incident_edges[e_Incoming]};
        }

        auto outgoing_edges() -> nano::ref_view<std::vector<edge_id_t>> {
            return nano::ref_view{m_incident_edges[e_Outgoing]};
        }

        [[nodiscard]] auto incoming_edges() const -> nano::ref_view<const std::vector<edge_id_t>> {
            return nano::ref_view{m_incident_edges[e_Incoming]};
        }

        [[nodiscard]] auto outgoing_edges() const -> nano::ref_view<const std::vector<edge_id_t>> {
            return nano::ref_view{m_incident_edges[e_Outgoing]};
        }

        [[nodiscard]] auto indegree() const -> node_id_t {
            return node_id_t{incoming_edges().size()};
        }

        [[nodiscard]] auto outdegree() const -> node_id_t {
            return node_id_t{outgoing_edges().size()};
        }

        [[nodiscard]] auto degree() const -> node_id_t {
            return indegree() + outdegree();
        }

        void add_incoming_edge(edge_id_t edge) {
            m_incident_edges[e_Incoming].push_back(edge);
        }

        void add_outgoing_edge(edge_id_t edge) {
            m_incident_edges[e_Outgoing].push_back(edge);
        }

        bool remove_edge(edge_id_t edge) {
            return nano::all_of(m_incident_edges, [edge](auto &range) { return remove_edge(edge, range); });
        }

        template<class Predicate>
        void remove_edges(Predicate &&pred) {
            nano::for_each(m_incident_edges,
                           [pred = std::forward<Predicate>(pred)](auto &range) { remove_edges(pred, range); });
        }

        auto incident_edges(EdgeDirection dir) const {
            return nano::ref_view{m_incident_edges[dir]};
        }

        auto incident_edges(EdgeDirection dir) {
            return nano::ref_view{m_incident_edges[dir]};
        }

        auto incident_edges() const {
            return nano::join_view(m_incident_edges);
        }

        auto incident_edges() {
            return nano::join_view(m_incident_edges);
        }

      private:
        template<class, class>
        friend struct Graph;

        static bool remove_edge(edge_id_t edge, std::vector<edge_id_t> &container) {
            auto pos = nano::find(container, edge);
            if (pos != std::end(container)) {
                *pos = container.back();
                container.pop_back();
                return true;
            } else {
                return false;
            }
        }

        template<class Predicate>
        static void remove_edges(Predicate pred, std::vector<edge_id_t> &container) {
            auto first_remove = nano::partition(container, std::not_fn(pred));
            container.erase(first_remove);
        }

        // With two member variables, the join_view does not work!
        std::array<std::vector<edge_id_t>, 2> m_incident_edges;
        using base = detail::empty_base<Label>;
    };
    template<>
    struct detail::GraphMember<e_Node> {
        template<class Label>
        using member_t = Node<Label>;
    };
}  // namespace graph
#endif  // GRAPH_NODE_HPP_D9A11A6CC27A45B69F037C6FD6FC1183
