#ifndef GRAPH_BASE_HANDLE_HPP_EEC9B54A7BA14AF096F84C4290EB0E45
#define GRAPH_BASE_HANDLE_HPP_EEC9B54A7BA14AF096F84C4290EB0E45

#include "helpers.hpp"
#include <type_traits>

namespace graph::detail {
    // These classes are not intended to be used for runtime polymorphism!
    // They only prevent code-duplication for the different handles.
    // Their destructors are protected non-virtual for a reason!
    template<class Graph, class Handled>
    struct BaseWrapped {
        using graph_t = Graph;
        using handled_t = maybe_const_t<Handled, std::is_const_v<graph_t>>;
        using id_t = typename handled_t::id_t;

      protected:
        template<class>
        friend struct BaseHandle;

        template<class, class>
        friend struct BaseWrapped;

        BaseWrapped(graph_t &graph, id_t id) : m_graph(graph), m_id(id) {}

        auto get_graph() const -> graph_t & {
            return m_graph;
        }

        auto get_id() const -> id_t {
            return m_id;
        }

        auto get_handled() const -> const handled_t & {
            return m_graph.get(m_id);
        }

        auto get_handled() -> handled_t & {
            return m_graph.get(m_id);
        }

        ~BaseWrapped() = default;
        graph_t &m_graph;
        id_t m_id;
    };

    template<class Wrapped>
    struct BaseHandle {
        using wrapped_t = Wrapped;
        using graph_t = typename wrapped_t::graph_t;
        using id_t = typename wrapped_t::id_t;

        template<class... Args, class = std::void_t<decltype(wrapped_t(std::declval<Args>()...))>>
        explicit BaseHandle(Args &&... args) : m_wrapped(std::forward<Args>(args)...) {}

        explicit BaseHandle(Wrapped wrapped) : m_wrapped(std::move(wrapped)) {}

        template<class OtherHandle, class = decltype(wrapped_t{std::declval<typename OtherHandle::wrapped_t>()})>
        BaseHandle(const OtherHandle &other) : BaseHandle(*other) {}

        auto get_graph() const -> graph_t & {
            return m_wrapped.get_graph();
        }

        auto get_id() const -> id_t {
            return m_wrapped.get_id();
        }

        auto operator*() const -> const wrapped_t & {
            return m_wrapped;
        }

        auto operator->() const -> const wrapped_t * {
            return &m_wrapped;
        }

        auto operator->() -> wrapped_t * {
            return &m_wrapped;
        }

      private:
        wrapped_t m_wrapped;
    };
}  // namespace graph::detail
#endif  // GRAPH_BASE_HANDLE_HPP_EEC9B54A7BA14AF096F84C4290EB0E45
