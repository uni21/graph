#ifndef GRAPH_NODE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71
#define GRAPH_NODE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71

#include "graph/detail/base_handle.hpp"
#include "graph/detail/helpers.hpp"
#include "../nanorange.hpp"
#include <type_traits>

namespace graph::detail {
    template<class Graph>
    struct NodeWrapped final : detail::BaseWrapped<Graph, typename Graph::node_t> {
        template<class Label = typename Graph::node_label_t, class = std::enable_if_t<!std::is_same_v<Label, void>>>
        auto label() const -> const Label & {
            return base_t::get_handled().label();
        }

        template<class Label = typename Graph::node_label_t,
                 class = std::enable_if_t<!std::is_const_v<Graph> && !std::is_same_v<Label, void>>>
        auto label() -> Label & {
            return base_t::get_handled().label();
        }

        [[nodiscard]] auto indegree() const -> node_id_t {
            return base_t::get_handled().indegree();
        }

        [[nodiscard]] auto outdegree() const -> node_id_t {
            return base_t::get_handled().outdegree();
        }

        [[nodiscard]] auto degree() const -> node_id_t {
            return base_t::get_handled().degree();
        }
        auto incoming_edges() const {
            return nano::transform_view(base_t::get_handled().incoming_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto incoming_edges() {
            return nano::transform_view(base_t::get_handled().incoming_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }
        auto outgoing_edges() const {
            return nano::transform_view(base_t::get_handled().outgoing_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto outgoing_edges() {
            return nano::transform_view(base_t::get_handled().outgoing_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto incident_edges(EdgeDirection dir) const {
            return nano::transform_view(base_t::get_handled().incident_edges(dir),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto incident_edges(EdgeDirection dir) {
            return nano::transform_view(base_t::get_handled().incident_edges(dir),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto incident_edges() const {
            return nano::transform_view(base_t::get_handled().incident_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

        auto incident_edges() {
            return nano::transform_view(base_t::get_handled().incident_edges(),
                                        [&graph = base_t::m_graph](auto edge_iter) { return graph[edge_iter]; });
        }

      private:
        friend struct detail::BaseHandle<NodeWrapped>;
        friend struct detail::BaseHandle<NodeWrapped<const Graph>>;
        friend struct NodeWrapped<const Graph>;

        NodeWrapped() = default;

        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        NodeWrapped(const NodeWrapped<OtherGraph> &other) : base_t(other.m_graph, other.m_id) {}
        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        NodeWrapped(NodeWrapped<OtherGraph> &&other) noexcept : base_t(other.m_graph, other.m_id) {}

        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        NodeWrapped &operator=(const NodeWrapped<OtherGraph> &other) {
            base_t::m_id = other.m_id;
        }
        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        NodeWrapped &operator=(NodeWrapped &&other) noexcept {
            base_t::m_id = other.m_id;
        }

        ~NodeWrapped() = default;

        using base_t = detail::BaseWrapped<Graph, typename Graph::node_t>;
        using base_t::base_t;
    };
}  // namespace graph::detail
#endif  // GRAPH_NODE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71
