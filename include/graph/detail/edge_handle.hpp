#ifndef GRAPH_EDGE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71
#define GRAPH_EDGE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71

#include "graph/detail/base_handle.hpp"
#include "graph/detail/helpers.hpp"

namespace graph::detail {
    template<class Graph>
    struct EdgeWrapped final : detail::BaseWrapped<Graph, typename Graph::edge_t> {
        template<class Label = typename Graph::edge_label_t>
        auto label() const -> Label {
            return base_t::get_handled().label();
        }

        template<class Label = typename Graph::edge_label_t,
                 class = std::enable_if_t<!std::is_const_v<Graph> && !std::is_same_v<Label, void>>>
        auto label() -> Label & {
            return base_t::get_handled().label();
        }

        auto from() const {
            return base_t::m_graph[base_t::get_handled().m_from];
        }

        auto to() const {
            return base_t::m_graph[base_t::get_handled().m_to];
        }

        template<class Func>
        auto apply_to_nodes(Func &&f) const {
            return _apply_to_nodes(*this, f);
        }

        template<class Func>
        auto apply_to_nodes(Func &&f) {
            return _apply_to_nodes(*this, f);
        }

      private:
        friend struct detail::BaseHandle<EdgeWrapped>;
        friend struct detail::BaseHandle<EdgeWrapped<const Graph>>;
        friend struct EdgeWrapped<const Graph>;

        EdgeWrapped() = default;

        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        EdgeWrapped(const EdgeWrapped<OtherGraph> &other) : base_t(other.m_graph, other.m_id) {}
        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        EdgeWrapped(EdgeWrapped<OtherGraph> &&other) noexcept : base_t(other.m_graph, other.m_id) {}

        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        EdgeWrapped &operator=(const EdgeWrapped<OtherGraph> &other) {
            base_t::m_id = other.m_id;
        }
        template<class OtherGraph, class = std::enable_if_t<std::is_convertible_v<OtherGraph, Graph>>>
        EdgeWrapped &operator=(EdgeWrapped &&other) noexcept {
            base_t::m_id = other.m_id;
        }

        template<class EdgeHandle, class Func, class Res = std::invoke_result_t<Func, typename Graph::node_handle_t>>
        static auto _apply_to_nodes(EdgeHandle _this, Func &&f)
            -> std::conditional_t<std::is_same_v<Res, void>, void, std::pair<Res, Res>> {
            if constexpr (std::is_same_v<Res, void>) {
                f(_this.from());
                f(_this.to());
            } else {
                return std::pair{f(_this.from()), f(_this.to())};
            }
        }
        ~EdgeWrapped() = default;

        using base_t = detail::BaseWrapped<Graph, typename Graph::edge_t>;
        using base_t::base_t;
    };

}  // namespace graph::detail
#endif  // GRAPH_EDGE_HANDLE_HPP_2EAD923C8ABE485984E47B220970CB71
