#ifndef GRAPH_GRAPH_MEMBERS_HPP_8F0A1BB164F44DDF806981E5302D580F
#define GRAPH_GRAPH_MEMBERS_HPP_8F0A1BB164F44DDF806981E5302D580F

#include "graph/common_defs.hpp"
namespace graph::detail {
    template<GraphMemberKind>
    struct GraphMember;
}

#endif  // GRAPH_GRAPH_MEMBERS_HPP_8F0A1BB164F44DDF806981E5302D580F
