#ifndef GRAPH_HELPERS_HPP_30966E2548604ABC83FA1262E39224CC
#define GRAPH_HELPERS_HPP_30966E2548604ABC83FA1262E39224CC

#include <iterator>
#include <type_traits>
#include "graph/common_defs.hpp"

namespace graph::detail {
    template<class T>
    struct empty_base {
        T data;
        template<class... Args>
        explicit empty_base(Args &&... args) : data(std::forward<Args>(args)...) {}
    };

    template<>
    struct empty_base<void> {
        empty_base() = default;
    };

    template<class T, bool isConst>
    using maybe_const_t = std::conditional_t<isConst, const T, std::remove_const_t<T>>;

    template<class T>
    using is_const_reference = std::conjunction<std::is_const<std::remove_reference_t<T>>, std::is_reference<T>>;

    template<class T>
    static constexpr bool is_const_reference_v = is_const_reference<T>::value;

    template<GraphMemberKind kind, class T_Node, class T_Edge>
    using enum_switch_t = std::conditional_t<kind == e_Node, T_Node, T_Edge>;
}  // namespace graph::detail
#endif  // GRAPH_HELPERS_HPP_30966E2548604ABC83FA1262E39224CC
