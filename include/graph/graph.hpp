#ifndef GRAPH_GRAPH_HPP_FC034D81A678407C9EC41A1B8E315F3C
#define GRAPH_GRAPH_HPP_FC034D81A678407C9EC41A1B8E315F3C

#include "common_defs.hpp"
#include "edge.hpp"
#include "handles.hpp"
#include "node.hpp"
#include "detail/helpers.hpp"
#include "nanorange.hpp"
#include <vector>

namespace graph {
    template<class NodeLabel = void, class EdgeLabel = void>
    struct Graph {
        template<GraphMemberKind kind>
        using label_t = detail::enum_switch_t<kind, NodeLabel, EdgeLabel>;
        template<GraphMemberKind kind>
        using member_t = typename detail::GraphMember<kind>::template member_t<label_t<kind>>;
        template<GraphMemberKind kind>
        using handle_t = graph::handle_t<kind, Graph>;
        template<GraphMemberKind kind>
        using const_handle_t = graph::handle_t<kind, const Graph>;

        using node_label_t = label_t<e_Node>;
        using edge_label_t = label_t<e_Edge>;
        using node_t = member_t<e_Node>;
        using edge_t = member_t<e_Edge>;
        using node_handle_t = handle_t<e_Node>;
        using const_node_handle_t = const_handle_t<e_Node>;
        using edge_handle_t = handle_t<e_Edge>;
        using const_edge_handle_t = const_handle_t<e_Edge>;

        auto nodes() const {
            return nano::transform_view(nano::iota_view{0ul, num_nodes().get()},
                                        [this](auto id) { return (*this)[node_id_t{id}]; });
        }

        auto nodes() {
            return nano::transform_view(nano::iota_view{0ul, num_nodes().get()},
                                        [this](auto id) { return (*this)[node_id_t{id}]; });
        }

        auto edges() const {
            return nano::transform_view(nano::iota_view{0ul, num_edges().get()},
                                        [this](auto id) { return (*this)[edge_id_t{id}]; });
        }

        auto edges() {
            return nano::transform_view(nano::iota_view{0ul, num_edges().get()},
                                        [this](auto id) { return (*this)[edge_id_t{id}]; });
        }

        template<GraphMemberKind kind>
        void reserve(id_t<kind> number) {
            if constexpr (kind == e_Node) {
                m_nodes.reserve(number.get());
            } else {
                m_edges.reserve(number.get());
            }
        }

        template<class... Args>
        node_id_t add_node(Args &&...args) {
            const auto id = num_nodes();
            m_nodes.emplace_back(std::forward<Args>(args)...);
            return id;
        }

        template<class... Args>
        edge_id_t add_edge(node_id_t from, node_id_t to, Args &&...args) {
            const auto id = num_edges();
            m_edges.emplace_back(from, to, std::forward<Args>(args)...);
            this->get(from).add_outgoing_edge(id);
            this->get(to).add_incoming_edge(id);
            return id;
        }

        template<GraphMemberKind kind>
        [[nodiscard]] auto num_members() const -> id_t<kind> {
            if constexpr (kind == GraphMemberKind::e_Node) {
                return num_nodes();
            } else {
                return num_edges();
            }
        }

        [[nodiscard]] auto num_nodes() const -> node_id_t {
            return node_id_t{m_nodes.size()};
        }

        [[nodiscard]] auto num_edges() const -> edge_id_t {
            return edge_id_t{m_edges.size()};
        }

        template<GraphMemberKind kind>
        [[nodiscard]] auto operator[](id_t<kind> id) const -> const_handle_t<kind> {
            return const_handle_t<kind>{*this, id};
        }

        template<GraphMemberKind kind>
        [[nodiscard]] auto operator[](id_t<kind> id) -> handle_t<kind> {
            return handle_t<kind>{*this, id};
        }

        void remove_edge(edge_id_t edge) {
            auto back_id = --num_edges();
            m_edges.back().apply_to_nodes(forward_from_id([&](auto &node) {
                for (auto &incident_edges: node.m_incident_edges) {
                    std::replace(begin(incident_edges), end(incident_edges), back_id, edge);
                }
            }));

            std::swap(get(edge), m_edges.back());

            m_edges.back().apply_to_nodes(forward_from_id([&](auto &node) { node.remove_edge(edge); }));

            m_edges.pop_back();
        }

        void move_edge(edge_id_t edge, node_id_t new_node, EdgeDirection dir) {
            const auto handle = (*this)[edge];
            const auto old_node_handle = dir == e_Outgoing ? handle->from() : handle->to();
            auto &old_node = get(old_node_handle.get_id());
            auto &edge_list = old_node.m_incident_edges[dir];
            const auto edge_pos = std::find(begin(edge_list), end(edge_list), edge);
            assert(edge_pos != end(edge_list));
            std::swap(*edge_pos, edge_list.back());
            edge_list.pop_back();

            get(new_node).m_incident_edges[dir].push_back(edge);
            if (dir == e_Outgoing) {
                get(edge).m_from = new_node;
            } else {
                get(edge).m_to = new_node;
            }
        }

      private:
        template<class, class>
        friend struct detail::BaseWrapped;

        template<GraphMemberKind kind>
        [[nodiscard]] auto get(id_t<kind> id) const -> const member_t<kind> & {
            if constexpr (kind == e_Node) {
                return m_nodes[id.get()];
            } else {
                return m_edges[id.get()];
            }
        }

        template<GraphMemberKind kind>
        [[nodiscard]] auto get(id_t<kind> id) -> member_t<kind> & {
            if constexpr (kind == e_Node) {
                return m_nodes[id.get()];
            } else {
                return m_edges[id.get()];
            }
        }

        template<class Func>
        auto forward_from_id(Func f) {
            return [=](auto id) { return f(this->get(id)); };
        }

        std::vector<node_t> m_nodes;
        std::vector<edge_t> m_edges;
    };
}  // namespace graph

#endif  // GRAPH_GRAPH_HPP_FC034D81A678407C9EC41A1B8E315F3C
