#include "graph/detail/helpers.hpp"
#include <catch2/catch.hpp>

TEST_CASE("Check if type is const reference", "[helpers]") {
    using namespace graph::detail;
    STATIC_REQUIRE(is_const_reference_v<const int &>);
    STATIC_REQUIRE(is_const_reference_v<volatile const int &>);
    STATIC_REQUIRE(is_const_reference_v<const int &&>);

    STATIC_REQUIRE_FALSE(is_const_reference_v<int &>);
    STATIC_REQUIRE_FALSE(is_const_reference_v<const int>);
    STATIC_REQUIRE_FALSE(is_const_reference_v<int &&>);
    STATIC_REQUIRE_FALSE(is_const_reference_v<int>);
}
