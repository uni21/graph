#include "graph/node.hpp"
#include "helper_types.hpp"
#include <catch2/catch.hpp>

TEMPLATE_TEST_CASE("test node templates", "[node]", void, int, helpers::non_default_constructible) {
    if constexpr (!std::is_same_v<TestType, helpers::non_default_constructible>) {
        graph::Node<TestType> node;
        REQUIRE(true);
    }
    if constexpr (!std::is_same_v<TestType, void>) {
        graph::Node<TestType> node(1);
        REQUIRE(true);
    }
}
