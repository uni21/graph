#ifndef GRAPH_HELPER_TYPES_HPP_6956720846A940B4A9BA4E767F1FC737
#define GRAPH_HELPER_TYPES_HPP_6956720846A940B4A9BA4E767F1FC737

namespace helpers {
    struct non_default_constructible {
        non_default_constructible() = delete;
        non_default_constructible(int) {}
    };
}  // namespace helpers

#endif  // GRAPH_HELPER_TYPES_HPP_6956720846A940B4A9BA4E767F1FC737
