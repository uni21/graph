#include "graph/edge.hpp"
#include "helper_types.hpp"
#include <catch2/catch.hpp>

TEMPLATE_TEST_CASE("test edge templates", "[edge]", void, int, helpers::non_default_constructible) {
    using namespace graph::literals;
    auto from = 0_node;
    auto to = 1_node;

    if constexpr (!std::is_same_v<TestType, helpers::non_default_constructible>) {
        graph::Edge<TestType> edge{from, to};
        REQUIRE(true);
    }
    if constexpr (!std::is_same_v<TestType, void>) {
        graph::Edge<TestType> edge(from, to, 1);
        REQUIRE(true);
    }
}