#include "graph/common_defs.hpp"
#include <catch2/catch.hpp>
#include <type_traits>

TEMPLATE_TEST_CASE("ids are strong typedefs", "[ids]", graph::node_id_t, graph::edge_id_t) {
    TestType id{5};

    STATIC_REQUIRE_FALSE(std::is_convertible_v<TestType, std::size_t>);
    REQUIRE(id == id);
    REQUIRE(id != id + TestType{1});
    REQUIRE(id < TestType{id.get() + 1});
    REQUIRE(id.get() == 5);
}

TEST_CASE("udls for ids", "[ids][udls]") {
    using namespace graph::literals;
    auto node_id = 5_node;
    auto edge_id = 5_edge;

    STATIC_REQUIRE(std::is_same_v<decltype(node_id), graph::node_id_t>);
    STATIC_REQUIRE(std::is_same_v<decltype(edge_id), graph::edge_id_t>);
    REQUIRE(node_id.get() == edge_id.get());
}
