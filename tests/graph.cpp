#include "graph/graph.hpp"
#include "helper_types.hpp"
#include <catch2/catch.hpp>

TEMPLATE_PRODUCT_TEST_CASE("Check adding new nodes and edges", "[graph]", (graph::Graph),
                           ((void, void), (int, int), (helpers::non_default_constructible, int) )) {
    using node_label_t = typename TestType::node_label_t;
    using edge_label_t = typename TestType::edge_label_t;

    auto from = graph::node_id_t{};
    auto to = graph::node_id_t{};
    auto edge1 = graph::edge_id_t{};
    auto edge2 = graph::edge_id_t{};

    TestType g;

    if constexpr (!std::is_constructible_v<node_label_t, int>) {
        from = g.add_node();
        to = g.add_node();
    } else {
        from = g.add_node(42);
        to = g.add_node(43);
    }

    REQUIRE(from.get() == 0);
    REQUIRE(to.get() == 1);

    if constexpr (!std::is_constructible_v<edge_label_t, int>) {
        edge1 = g.add_edge(from, to);
        edge2 = g.add_edge(to, from);
    } else {
        edge1 = g.add_edge(from, to, 10);
        edge2 = g.add_edge(to, from, 10);
    }

    REQUIRE(edge1.get() == 0);
    REQUIRE(edge2.get() == 1);

    REQUIRE(g.num_nodes().get() == 2);
    REQUIRE(g.num_edges().get() == 2);
}
