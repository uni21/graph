#include "graph/graph.hpp"
#include <catch2/catch.hpp>
using namespace graph::literals;

TEST_CASE("Test node handles", "[node_handle]") {
    auto g = graph::Graph{};
    g.add_node();
    g.add_node();
    g.add_edge(0_node, 1_node);

    auto node_handle = std::as_const(g)[0_node];

    REQUIRE(node_handle->degree() == 1_node);
    REQUIRE(g[1_node]->degree() == 1_node);

    for (auto handle: g.nodes()) {
        REQUIRE(handle->outdegree() == 1_node - handle.get_id());
        REQUIRE(handle->indegree() == handle.get_id());
    }
    auto incident = node_handle->incident_edges();
    for (auto handle: incident) {
        REQUIRE(handle->from().get_id() == 0_node);
    }

    REQUIRE((*begin(incident))->from().get_id() == 0_node);
    REQUIRE((*begin(incident)).get_id() == 0_edge);
}
